# Java ArrayList | How to Sort ArrayList and ArrayList of Object in JAVA?

[![Java ArrayList | How to Sort ArrayList and ArrayList of Object in JAVA?](https://youtu.be/oBrOmRV-dy4/0.jpg)](https://youtu.be/oBrOmRV-dy4)

*__Kindly Like, Subscribe and Share the YouTube Video__*

## How to Sort ArrayList and ArrayList of Object in JAVA?

*Feel free to comment and let us know what kind video you would like to watch*

1. Collections.sort

```
Collections.sort(stringList);
System.out.println(stringList);
Collections.sort(stringList, Collections.reverseOrder());
```

2. stream().sorted()

```
List<String> sortedStringList = stringList.stream().sorted().collect(
                Collectors.toList());
```

3.1 Comparator

```
Comparator<LiveCodeMember> idComparator = new Comparator<LiveCodeMember>() {
    @Override
    public int compare(LiveCodeMember o1, LiveCodeMember o2) {
        if (o1.getId() > o2.getId()) {
            return 1;
        } else if (o1.getId() < o2.getId()) {
            return -1;
        } else {
            return 0;
        }

    }
};
liveCodeMemberList.sort(idComparator);
```

3.2 Comparator Lambda 

```
Comparator<LiveCodeMember> ageComparator = (a, b) -> Integer
        .compare(b.getAge(), a.getAge());
liveCodeMemberList.sort(ageComparator);

ageComparator = Comparator.comparingInt(LiveCodeMember::getAge);
liveCodeMemberList.sort(ageComparator);
```

